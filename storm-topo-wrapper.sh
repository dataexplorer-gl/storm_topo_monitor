#!/bin/bash
#
# Author: Hari Chintala, August-2017
# 
# This is a Check Config Service to initiate/stop and restart 
# Storm-Monitoring Application automatically on background
#
# /etc/init.d/storm-topo-monitor
#
### END INIT INFO
. /etc/bashrc

# Below user should have access rights to Storm API
STORM_API_USER=storm
# Activate the python virtual environment (using conda env name - "StormTopoMonitor")
#source activate StormTopoMonitor
# Export Python Executable (This should be Virtula Conda Env)
echo -e "Print Python EXE====>>" `which python` 
export PYTHON_EXE=`which python`

exec sudo -u ${STORM_API_USER} /opt/wakari/anaconda/envs/StormTopoMonitor/bin/python /usr/share/storm-topo-monitor/stormMonitoring.py start
