#
# Description: stormMonitoring is used to monitor Storm Topologies for 
#              status/Tuple processing, can be extended future 
# Owner      : Hari Chintala 
# How to Run : stormMonitoring can be initiated as a unix daemon,  you
#              could below command to start the Daemon
#              python stormMonitoring.py start/stop/restart  
#
# Deamon Log : /var/log/storm-monitoring
# Current Version:  1.0
#
# Version History
# ---------------
# Version #  Date        Author                 Amendment History
# ---------  ----        -------------          ------------------
# 1.0        09/08/2017  Hari Chintala          Initial Version
########################################################################
# -*- coding: utf-8 -*-

print "Inside the SCRIPT - HELLO"

import logging
import time
from daemon import runner
import requests
import json
import argparse
import pprint
import time
import pycurl
from io import BytesIO
from requests_kerberos import HTTPKerberosAuth
from terminaltables import AsciiTable
import socket
import time
import os
import sys
import ConfigParser

# Autheticate/renew Kerberos Keytab
os.system("kinit -kt keytabs/storm.keytab xyz_host@ABC.COM")

# Declare Global variables
global topologySummary
global expectedTopologies

def script_path():
    baseDir=os.path.dirname(os.path.realpath(sys.argv[0]))
    return baseDir

#assert os.path.exists("monitoringConf.ini")
# Parse variables from the config file
scriptPath=script_path()
config = ConfigParser.ConfigParser()
config.read(scriptPath + "/conf/monitoringConf.ini")

tupleCheckInterval=config.getfloat("monitoringvars", 'interval')
nimbusServer=config.get("monitoringvars", "nimbusServer")
nimbusPort=config.get("monitoringvars", "nimbusPort")
expectedTopologies=eval(config.get("monitoringvars", "expectedTopologies"), {}, {})
kerberosKeytab=config.get("monitoringvars", "stormKeytab")

# Autheticate/renew Kerberos Keytab
os.system("kinit -kt /etc/security/keytabs/storm.headless.keytab " + kerberosKeytab)

# Form the url from the variables
url_summary = "http://" + nimbusServer + ":" + nimbusPort + "/api/v1/topology/summary"
print url_summary

# Topology Summary
# Kerberos HTTP Authetication
def topoInfo():
    resp = requests.get(url_summary, auth=HTTPKerberosAuth())
    resp.encoding = 'utf-8'
    global topologySummary
    topologySummary = resp.json()
# Debug
    logger.debug( ">>>> RESPONSE <<<<" + str(resp) + ">>>> END RESPONSE <<<<" )

# Get most recent number of emitted tuples for that topology or 0 on nrror
def getTopoTuples(topoID):
    url = "http://" + nimbusServer + ":" + nimbusPort + "/api/v1/topology/"+topoID
    logger.info(url)
    resp = requests.get(url, auth=HTTPKerberosAuth())
    resp.encoding = 'utf-8'
    data = resp.json()
    try:
    	statusTopo = data['status']
    except:
        statusTopo = 'Uknown'
    
    #If the topology ID does not exist, return 0
    if str(statusTopo) != 'ACTIVE':
	 logger.info('Topology "'+topoID+'" Status ERROR!')
	 return 0
    else:
         logger.info('ALL Okay!' + topoID+ ':' + statusTopo) 

    #Return number of emitted tuples for "all time"
    try:
    	return data['topologyStats'][3]['emitted']
    except IndexError:
        return 'Updating'


# Returns a dictionary of tuple id and the number of its current emitted tuples
def updateTupleCounts(data):
    global unicodeActiveTopologies
    tuplesEmitted = {}
    unicodeActiveTopologies = ()
    for topology in data['topologies']:
        topologyID = topology['id']
        tuplesEmitted[topologyID] = getTopoTuples(topologyID)
        # This is for activeTopostatus function, writing all the active Topo's
        unicodeActiveTopologies += (topologyID,)
    return tuplesEmitted

def activeTopostatus():
    activeTopologiesTup=tuple(map(str, unicodeActiveTopologies))
    activeTopologies=map(lambda each:each.strip('-0123456789'), activeTopologiesTup)
# Debug
#    print "PPPPPPPPPPPPPPPP>>>>>>>>>ACTIVE", activeTopologies
#    print "PPPPPPPPPPPPPPPP>>>>>>>>>EXPECTED", expectedTopologies
    inactiveTopologies = [x for x in expectedTopologies if x not in activeTopologies]
    if len(inactiveTopologies) > 0:
       logger.error("\n \nFATAL: Expected Active Topologies found In-active, Please Investigate\n----------------------------------------------------------------------\n" + "\n".join(inactiveTopologies) + "\n \n \n \n**** Note: Please add topologyID to exclusion list in order to blacklist") 
    else:
       logger.info("all Topologies working okay")


def finalCount():
    table = AsciiTable(table_data)
    final = table.table
    if len(failedTopos) > 0:
    	logger.error("\n \n \nFollowing Topologies Stopped Processing Tuples ---->>> \n+-------------------------------------------------------+\n" + "\n".join(map(str, failedTopos)) + "\n \n \n<=== TupleCount by Topologies (Time Window: " + str(tupleCheckInterval) + " Seconds) ===>\n" + final)
    else:
    	logger.info("\n \n \nAll Tuples Processed fine\n+-------------------------------------------------------+\nTupleCount by Topologies (Time Window: " + str(tupleCheckInterval) + " Seconds) ===>\n" + final)
def tabTuples():
    table_data.append([key,str(oldTupleCount),str(newTupleCount)])

def clearTab():
    del table_data[:]



#To debug - Run outside initctl/upstart, rename "null" to "tty"
class App():
    def __init__(self):
	self.stdin_path='/dev/null'
	self.stdout_path ='/dev/null'
	self.stderr_path ='/dev/null'
	self.pidfile_path ='/var/run/storm-topo-monitor/storm-topo-monitor.pid'
	self.pidfile_timeout =5

    def run(self):
        while True:
            #Main code goes here ...
            #Note that logger level needs to be set to logging.DEBUG before this shows up in the logs
            logger.debug("Debug message")
	    logger.info("Info message")
            logger.warn("Warning message")
            logger.error("Error message")

# Prime the loop by getting the current list of tuple ids and emitted count, then wait for interval time
	    topoInfo()
            oldTupleCountDict = updateTupleCounts(topologySummary)
#table_data = [['Topology_ID','Old Tuple Count','New Tuple Count']]
# Prime the loop by waiting the interval for the tuples to increase
            time.sleep(tupleCheckInterval)
# Constantly check to see if topologies have stopped receiving tuples
            while True:
                global failedTopos
                failedTopos=[]
                global table_data 
                table_data = [['Topology_ID','Old Tuple Count','New Tuple Count']]
                newTupleCountDict = updateTupleCounts(topologySummary)
    # Alert if any Expected Topology is not Active
                activeTopostatus()
    # iterate through oldTuple list, alert if same number of all time tuples were emitted
                global key 
		global oldTupleID 
		global oldTupleCount
		global newTupleCount
                for key in oldTupleCountDict:
                     oldTupleID = key
                     oldTupleCount = oldTupleCountDict[key]
                     newTupleCount = newTupleCountDict[oldTupleID]
        #Add the old and new count to the table
                     tabTuples()
        # If tuple count has not increased
                     if newTupleCount <= oldTupleCount:
                        # alertTopo(oldTupleID)
                         failedTopos.append([oldTupleID])
                     else:
                         logger.info('DEBUG: "' + oldTupleID + '" continues to run properly')

    # TODO Consider removing topologies from oldTupleCountDict when alert is triggered?

    # Print Final Table with the Count
                finalCount()
    		clearTab()
    # Update oldTupleCountDict to reflect the most recent count data
    		topoInfo()
    		oldTupleCountDict = updateTupleCounts(topologySummary)
    # Delay loop for interval time period
    		time.sleep(tupleCheckInterval)

app = App()
logger = logging.getLogger("TopoMonitorDaemon")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/var/log/storm-monitoring/storm-monitoring-daemon.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

daemon_runner = runner.DaemonRunner(app)
#This ensures that the logger file handle does not get closed during daemonization
daemon_runner.daemon_context.files_preserve=[handler.stream]
daemon_runner.do_action()
