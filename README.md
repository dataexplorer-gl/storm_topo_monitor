### Storm Topo Monitoring API/Daemon

Daemon is based on Python and has the functionality to monitor:

1) Topology Status
2) Tuples Processed status

* Can be extended


### Pre-Requsites

1) Note: Script running Server Should have access to the STORM UI Server(Ports)
2) Should have Anaconda/miniconda client installed (has access to Continuum Anaconda Repo)
3) sudo to Root and Install below conda packages:

**Install Conda Packages (you could use pip if you dont have conda/ but have to change default path to pyton in storm-topo-monitor.conf)**<br/></br>
	[ ]conda install requests-kerberos -y<br/> 
	[ ]conda install terminaltables -y<br/> 

4) Create Necessary Directories:<br/> 

	[ ]mkdir -p /var/run/storm-topo-monitor<br/> 
 	[ ]mkdir -p /var/log/storm-topo-monitor<br/> 
	[ ]mkdir -p /var/log/storm-monitoring<br/> 
	[ ]mkdir -p /usr/share/storm-topo-monitor<br/> 

5) Give Required Permissions:<br/>

	[ ]chmod -R 777 /var/log/storm-topo-monitor<br/> 
	[ ]chmod -R 777 /var/run/storm-topo-monitor<br/> 
	[ ]chmod -R 777 /var/log/storm-monitoring<br/> 
	[ ]chmod -R 777 storm-topo-monitor<br/> 


### START/STOP DAEMON :<br/>

1) Copy the FIles to Upstart/Initctl directory<br/>
*Connect as Root<br/>

	[ ]Copy the Storm Repo directory/files to /usr/share/storm-topo-monitor<br/> 
	[ ]Copy /usr/share/storm-topo-monitor/conf/storm-topo-monitor.conf to /etc/init<br/> 

**Location: /etc/init**

2) Start Stop Script, Unless issues "stop" script would never die as it re-spawns<br/><br/>

	**Run Command  - _initctl start storm-topo-monitor_**<br/> 
	**Stop Command - _initctl stop storm-topo-monitor_**<br/> 

### Configuration Management:

Edit the file monitoringConf.ini under conf folder for any configuration changes

<br/>
<br/>

*******************************************************************************
### To DEBUG the script, change Daemon script as below stormMonitoring.py 
******************************************************************************* 

Inside the storMonitoring.py file change as below:

	self.stdin_path='/dev/null'
	self.stdout_path ='/dev/null'
	self.stderr_path ='/dev/null'

     TO
   
        self.stdin_path='/dev/tty'
	self.stdout_path ='/dev/tty'
	self.stderr_path ='/dev/tty'

## For Debugging ONLY, you could start script as below

	python /usr/share/stormMonitoring.py start
	python /usr/share/stormMonitoring.py stop


NOTE: Change /dev/null to /dev/tty to start the script from initctl 
********************************************************************************

